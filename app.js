const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
require('dotenv/config');

app.use(bodyParser.json());

// Routes
const postsServ = require('./service/post');
const getAllRoleServ = require('./service/role/getAllRole');
const saveNewRole = require('./service/role/saveNewRole');

app.use('/app', postsServ);
app.use('/app', getAllRoleServ);
app.use('/app', saveNewRole);

// Database
mongoose.connect(
   process.env.DB_CONN,
   {useNewUrlParser: true, useUnifiedTopology: true},
   () => console.log('Conexion exitosa'))

app.listen(3000);
