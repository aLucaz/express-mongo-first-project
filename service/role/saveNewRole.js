const express = require('express');
const router = express.Router();
const RoleModel = require('../../model/role');

router.post('/save-new-role', (async (req, res) => {
   console.log(req.body);
   const role = new RoleModel({
      name: req.body.name,
      description: req.body.description
   });

   try {
      const savedRole = await role.save();
      res.json(savedRole);
   } catch (reason) {
      res.json({message: reason})
   }
}))

module.exports = router;
