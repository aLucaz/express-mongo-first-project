const express = require('express');
const router = express.Router();
const roleModel = require('../../model/role');

router.get('/get-all-roles', ( async (req, res) => {
   try {
      const roles = await roleModel.find();
      res.json(roles);
   }catch (reason) {
      res.json(reason)
   }
}))

module.exports = router;
